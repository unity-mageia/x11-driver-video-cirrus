Name: x11-driver-video-cirrus
Version: 1.5.3
Release: %mkrel 9
Summary: X.org driver for Cirrus Logic
Group: System/X11
URL: http://xorg.freedesktop.org
Source: http://xorg.freedesktop.org/releases/individual/driver/xf86-video-cirrus-%{version}.tar.bz2
Patch0: 0001-Disable-acceleration-under-qemu.patch
Patch1: 0002-Use-16bpp-when-running-in-virt-and-on-XenSource-gfx.patch
Patch2: 0003-Remove-almost-no-op-setup-functions.patch
Patch3: 0004-Don-t-build-split-alpine-and-laguna-support.patch
Patch100: 0001-Adapt-Block-WakeupHandler-signature-for-ABI-23.patch 
License: MIT
BuildRequires: x11-proto-devel >= 1.0.0
BuildRequires: pkgconfig(xorg-server) >= 1.0.1
BuildRequires: x11-util-macros >= 1.0.1
Conflicts: xorg-x11-server < 7.0
Requires: x11-server-common

%description
x11-driver-video-cirrus is the X.org driver for Cirrus Logic.

%prep
%autosetup -p1 -n xf86-video-cirrus-%{version}

%build
autoreconf -vfi
%configure2_5x
%make_build

%install
%make_install

%files
%doc COPYING
%{_libdir}/xorg/modules/drivers/cirrus_drv.la
%{_libdir}/xorg/modules/drivers/cirrus_drv.so
%{_mandir}/man4/cirrus.*

